package com.metamorphosys.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler{

	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		/*SpringWebContext	context = StandalonSpringThymeleafUtils.createContext(request, response);
		context.setVariable("flag", true);
		System.out.println("exception====="+exception.getMessage());
		System.out.println("exception==111==="+response.getWriter());*/
		//request.getRequestDispatcher(String.format("/error?message=%s", message)).forward(request, response);
		request.getRequestDispatcher(String.format("/loginFailure?message=%s", exception.getMessage())).forward(request, response);
		//StandalonSpringThymeleafUtils.render("user/login", context, response.getWriter());
		// TODO Auto-generated method stub
		
	}

	

	

	

}
