package com.metamorphosys.user;

import java.security.InvalidAlgorithmParameterException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.metamorphosys.insureconnect.dataobjects.transaction.UserRoleDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.UsersDO;
import com.metamorphosys.insureconnect.jpa.transaction.UserRepository;
import com.metamorphosys.user.controller.EncryptDecryptClass;
import com.metamorphosys.user.security.SecurityUtil;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);

	public CustomAuthenticationProvider() {
		logger.info("*** CustomAuthenticationProvider created");
	}

	@Autowired
	UserRepository userRepository;
	@Autowired
	EncryptDecryptClass encryptDecryptClass;

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date date = new Date();
		UsersDO userDO= userRepository.findByUserId(authentication.getName());
		if(userDO!=null)
		{
			String passwordStoredInDB = "";
			String passwordFromClient = "";

			SecurityUtil aesUtil = null;
			try {
				aesUtil = new SecurityUtil(128, 1000);
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				passwordStoredInDB = decryptPassword(userDO.getPassword(), aesUtil);
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				passwordFromClient = decryptPassword(authentication.getCredentials().toString(), aesUtil);
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			if(!passwordFromClient.equals(passwordStoredInDB))
			{
				userDO.setNoOfFailedAttempt(userDO.getNoOfFailedAttempt()==null?1:(userDO.getNoOfFailedAttempt()+1));
				userRepository.save(userDO);
				throw new BadCredentialsException("Invalid password");
			}else if(userDO.getUserStatus()==null ||userDO.getUserStatus().equals("INACTIVE"))
			{
				userDO.setNoOfFailedAttempt(userDO.getNoOfFailedAttempt()==null?1:(userDO.getNoOfFailedAttempt()+1));
				userRepository.save(userDO);

				throw new DisabledException("User status InActive");
			}else if(userDO.getUserStatus().equals("LOCKED"))
			{
				userDO.setNoOfFailedAttempt(userDO.getNoOfFailedAttempt()==null?1:(userDO.getNoOfFailedAttempt()+1));
				userRepository.save(userDO);
				throw new DisabledException("User status locked");
			}else if(userDO.getNoOfFailedAttempt()!=null && userDO.getNoOfFailedAttempt()>5)
			{
				userDO.setNoOfFailedAttempt(userDO.getNoOfFailedAttempt()==null?1:(userDO.getNoOfFailedAttempt()+1));
				userRepository.save(userDO);
				throw new DisabledException("Max Login Attempt Exceeded ");
			}else if(userDO.getAccountEndDt()!= null && date.before(userDO.getAccountEndDt())){
				userDO.setNoOfFailedAttempt(userDO.getNoOfFailedAttempt()==null?1:(userDO.getNoOfFailedAttempt()+1));
				userRepository.save(userDO);
				throw new AccountExpiredException("Account Expired");

			}else{
				List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
				if(userDO.getUserRoleDOList()!=null && userDO.getUserRoleDOList().size()>0)
					for(UserRoleDO roleDO:userDO.getUserRoleDOList()){
						grantedAuths.add(new SimpleGrantedAuthority(roleDO.getRoleId()));
					}
				userDO.setLastloginDt(new Timestamp(date.getTime()));

				userRepository.save(userDO);
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpSession session= attr.getRequest().getSession(true);
				session.setAttribute("user", userDO);
				// attr.getRequest().getSession().setMaxInactiveInterval(600);
				return new UsernamePasswordAuthenticationToken(authentication.getName(), passwordFromClient, grantedAuths);
			} 
		}
		else{

			throw new UsernameNotFoundException("User does nor exist");
		}


	}

	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	public String decryptPassword(String encryptedPWD, SecurityUtil aesUtil) throws NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException{
		String[] ecryptedPassword = encryptedPWD.split("__babacd_dcabab__");
		//		int keySize = Integer.parseInt(ecryptedPassword[ecryptedPassword.length-1]);
		//		int ic = Integer.parseInt(ecryptedPassword[ecryptedPassword.length-2]);
		String salt = ecryptedPassword[2];
		String iv = ecryptedPassword[1];
		String pp = ecryptedPassword[3];
		String cipherText = ecryptedPassword[0];

		String decryptedPassword =  aesUtil.decrypt( salt, iv, pp, cipherText);

		return decryptedPassword;
	}




}
