package com.metamorphosys.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@SpringBootApplication(scanBasePackages={"com.metamorphosys.user"})
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;

	@Autowired
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(this.customAuthenticationProvider);
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.authorizeRequests()
				.antMatchers("/assets/**", "/authenticate/**", "interface/**", "/activity/**", "/dashboard/**")
				.permitAll().antMatchers(HttpMethod.POST, "/api/transferPolicy").permitAll()
				.antMatchers(HttpMethod.POST, "/activity/upload").permitAll()
				.antMatchers(HttpMethod.POST, "/api/copyCase").permitAll()
				.antMatchers(HttpMethod.GET, "/activity/download").permitAll()
				.antMatchers(HttpMethod.GET, "/downloadDB/sqlite").permitAll()
				.antMatchers(HttpMethod.GET, "/downloadDB/sqlite/usahaku").permitAll()
				.antMatchers(HttpMethod.POST, "/interface/policyInterface").permitAll()
				.antMatchers(HttpMethod.POST, "/interface/ali/policyInterface").permitAll()
				.antMatchers(HttpMethod.POST, "/api/ali/transferPolicy").permitAll()
				.antMatchers(HttpMethod.POST, "/interface/uploadFile").permitAll()
				.antMatchers(HttpMethod.GET, "/interface/validateToken").permitAll()
				.antMatchers(HttpMethod.GET, "/interface/syncVehicles").permitAll()
				.antMatchers(HttpMethod.GET, "/interface/inValidateToken").permitAll();
		http.authorizeRequests().anyRequest().authenticated().and().formLogin().defaultSuccessUrl("/menuPage", true)
				.loginPage("/login").permitAll()
				.failureUrl("/loginFailure").and().logout().invalidateHttpSession(true).logoutUrl("/signout")
				.deleteCookies("JSESSIONID").permitAll().and().sessionManagement().maximumSessions(1)
				.expiredUrl("/error")
				.sessionRegistry(sessionRegistry()).and().invalidSessionUrl("/");
	}

	@Bean
	SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
				.antMatchers("/assets/**", "/login.html", "/authenticate/**", "/interface/**", "/activity/**",
						"/dashboard/**")
				.antMatchers(HttpMethod.POST, "/api/transferPolicy")
				.antMatchers(HttpMethod.POST, "/api/ali/transferPolicy")
				.antMatchers(HttpMethod.POST, "/interface/ali/policyInterface")
				.antMatchers(HttpMethod.POST, "/activity/upload")
				.antMatchers(HttpMethod.POST, "/api/copyCase").antMatchers(HttpMethod.GET, "/activity/download", "GET")
				.antMatchers(HttpMethod.GET, "/downloadDB/sqlite", "GET")
				.antMatchers(HttpMethod.GET, "/downloadDB/sqlite/usahaku", "GET")
				.antMatchers(HttpMethod.POST, "/interface/policyInterface")
				.antMatchers(HttpMethod.POST, "/interface/uploadFile")
				.antMatchers(HttpMethod.GET, "/interface/validateToken")
				.antMatchers(HttpMethod.GET, "/interface/inValidateToken")
				.antMatchers(HttpMethod.GET, "/interface/syncVehicles");
	}

	@Bean
	public static ServletListenerRegistrationBean httpSessionEventPublisher() {
		return new ServletListenerRegistrationBean(new HttpSessionEventPublisher());
	}

	@Bean
	public AuthenticationFailureHandler CustomAuthenticationFailureHandler() {
		SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler();
		return handler;
	}
}