package com.metamorphosys.user.controller;

import java.security.InvalidAlgorithmParameterException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.metamorphosys.insureconnect.dataobjects.master.ApplicationDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDeviceMappingDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.DeviceDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.UsersDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.transaction.AgentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.DeviceRepository;
import com.metamorphosys.insureconnect.jpa.transaction.UserRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.user.security.SecurityUtil;
import com.metamorphosys.user.util.SerializationUtility;

@RestController
@RequestMapping("/authenticate")
public class AuthController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	AgentRepository agentRepository;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	DeviceRepository deviceRepository;

	@CrossOrigin
	@RequestMapping(value = "/authRequest", method = RequestMethod.GET)
	ResponseEntity authenicateRequest(@RequestParam("userid") String userId, @RequestParam("password") String password,
			@RequestParam("deviceid") String deviceid, HttpServletRequest request) throws NoSuchPaddingException, InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		LOGGER.info("Auth Request Received");
		
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		HashMap<String, String> map = new HashMap<String, String>();
		
		String[] ecryptedPassword = password.split("__babacd_dcabab__");
		String salt = ecryptedPassword[2];
		String iv = ecryptedPassword[1];
		String pp = ecryptedPassword[3];
		String cipherText = ecryptedPassword[0];

		SecurityUtil aesUtil = new SecurityUtil(128, 1000);
		
        String decryptedPassword =  aesUtil.decrypt( salt, iv, pp, cipherText);
		String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.AUTHINTERFACEPATH).getValue()
				+ "/" + userId + "/" + decryptedPassword;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> reponseObject = restTemplate.exchange(interfaceUrl, HttpMethod.GET, null, String.class);
		if (HttpStatus.OK.equals(reponseObject.getStatusCode())) {
			HashMap<String, String> hashMap = (HashMap<String, String>) SerializationUtility.getInstance()
					.fromJson(reponseObject.getBody(), HashMap.class);
			if (InsureConnectConstants.InterfaceStatusCd.SUCCESS.equals(hashMap.get("statusCode"))) {
				httpHeaders.add("Authorization",hashMap.get("authToken"));
				httpHeaders.add("Access-Control-Expose-Headers","Authorization");
				AgentDO agentDetail = (AgentDO) SerializationUtility.getInstance().fromJson(hashMap.get("AgentDO"),
						AgentDO.class);
				if (InsureConnectConstants.Channel.AGENCY.equals(request.getHeader("channel"))
						&& InsureConnectConstants.Source.MOBILE.equals(request.getHeader("source"))) {
					
					AgentDO agentDO = null;
					String channel = request.getHeader("channel").toUpperCase();
					agentDO = agentRepository.findByUserIdAndChannel(userId.toLowerCase(), channel);

					UsersDO user = userRepository.findByUserIdAndChannel(userId.toLowerCase(), channel);
					boolean isValid = false;
					if (agentDO == null) {
						agentDO = agentDetail;
						// agentDO.setUserId(AgentDetail.getUserId());
						agentDO.setSource(request.getHeader("source"));
						agentDO.setChannel(request.getHeader("channel"));
					} else {
						agentDO.setAgentId(agentDetail.getAgentId());
						agentDO.setLastloginDt(agentDetail.getLastloginDt());
						agentDO.setAgentFirstName(agentDetail.getAgentFirstName());
						agentDO.setMobileNumber(agentDetail.getMobileNumber());
						agentDO.setEmailId(agentDetail.getEmailId());
						agentDO.setLicenseExpiryDt(agentDetail.getLicenseExpiryDt());
						agentDO.setAgentCategory(agentDetail.getAgentCategory().toUpperCase());

						if (agentDO.getAgentDeviceMappingDOList().size() > 0) {
							for (AgentDeviceMappingDO deviceMappingDO : agentDO.getAgentDeviceMappingDOList()) {
								if (deviceid.equals(deviceMappingDO.getDeviceId())) {
									deviceMappingDO.setLastOnlineDt(new Timestamp(System.currentTimeMillis()));
									if(Integer.parseInt(request.getHeader("policyseq")) > 0){
										deviceMappingDO.setPolicyseq(Integer.parseInt(request.getHeader("policyseq")));
									}
									if(Integer.parseInt(request.getHeader("quotationseq")) > 0){
										deviceMappingDO.setQuotationseq(Integer.parseInt(request.getHeader("quotationseq")));
									}
									
									agentDO.setLastOnlineDt(deviceMappingDO.getLastOnlineDt());
									agentDO.setDeviceUserSeq(deviceMappingDO.getId().toString());
									
									agentDO.setPolicyseq(deviceMappingDO.getPolicyseq());
									agentDO.setQuotationseq(deviceMappingDO.getQuotationseq());
									isValid = true;
									break;
								}
							}
						}
					}
					if (!isValid) {
						AgentDeviceMappingDO agentDevice1 = new AgentDeviceMappingDO();
						agentDevice1.setDeviceId(deviceid);
						List<AgentDeviceMappingDO> agentDeviceMappingDOs = new ArrayList<AgentDeviceMappingDO>();
						
						if(agentDO !=null)
						{
							if (agentDO.getAgentDeviceMappingDOList() != null) {
								for (AgentDeviceMappingDO deviceMappingDO : agentDO.getAgentDeviceMappingDOList()) {
									agentDeviceMappingDOs.add(deviceMappingDO);
								}
							}
						}
						// to be added from thirdParty
						agentDevice1.setAgentId(agentDO.getAgentId());
						agentDevice1.setLastOnlineDt(new Timestamp(System.currentTimeMillis()));
						agentDevice1.setLastDownloadDt(new Timestamp(System.currentTimeMillis()));
						agentDO.setLastOnlineDt(agentDevice1.getLastOnlineDt());
						agentDevice1.setPolicyseq(Integer.parseInt(request.getHeader("policyseq")));
						agentDevice1.setQuotationseq(Integer.parseInt(request.getHeader("quotationseq")));
						agentDeviceMappingDOs.add(agentDevice1);
						agentDO.setAgentDeviceMappingDOList(agentDeviceMappingDOs);
						agentDO.setAgentStatusCd("AGENTACTIVE");
						agentDO.setLastOnlineDt(agentDevice1.getLastOnlineDt());
						agentDO.setPolicyseq(agentDevice1.getPolicyseq());
						agentDO.setQuotationseq(agentDevice1.getQuotationseq());
						DeviceDO deviceDO = deviceRepository.findByDeviceId(deviceid);
						if (deviceDO == null) {
							deviceDO = new DeviceDO();
							deviceDO.setDeviceId(deviceid);
							deviceRepository.save(deviceDO);
						}
					}
					agentRepository.save(agentDO);
					if (agentDO.getDeviceUserSeq() == null) {
						for (AgentDeviceMappingDO agentDeviceMappingDO : agentDO.getAgentDeviceMappingDOList()) {
							if (deviceid.equals(agentDeviceMappingDO.getDeviceId())) {
								agentDO.setDeviceUserSeq(agentDeviceMappingDO.getId().toString());
								break;
							}
						}
					}

					if (user == null) {
						user = new UsersDO();
						user.setUserId(agentDO.getUserId());
						user.setChannel(request.getHeader("channel"));
						// user.setPassword(agentDO.getPassword());
						userRepository.save(user);
					}
					String agentJson = SerializationUtility.getInstance().toJson(agentDO);
					JsonParser jsonParser = new JsonParser();
					JsonObject jsonObject = (JsonObject) jsonParser.parse(agentJson);
					jsonObject.remove("agentDeviceMappingDOList");
					map.put("agent", jsonObject.toString());
					List<ApplicationDO> applicationDOs = applicationRepository
							.findByApplicationType(InsureConnectConstants.OFFLINE);
					map.put("application", SerializationUtility.getInstance().toJson(applicationDOs));
					String toJson = SerializationUtility.getInstance().toJson(map);
					LOGGER.info("Sending Response");
					return new ResponseEntity(toJson, httpHeaders, httpStatus);
				} else if (InsureConnectConstants.Channel.BANCA.equals(request.getHeader("channel"))
						&& InsureConnectConstants.Source.MOBILE.equals(request.getHeader("source"))) {
					UsersDO usersDO2 = userRepository.findByUserIdAndChannel(userId, request.getHeader("channel"));

					if (usersDO2 == null) {
						LOGGER.info("Sending Response");
						return null;
					} else {
						String toJson = SerializationUtility.getInstance().toJson(usersDO2);
						LOGGER.info("Sending Response");
						return new ResponseEntity(toJson, httpHeaders, httpStatus);
					}
				}
			} else {
				if (reponseObject.getBody() != null) {
					map.put("agent", SerializationUtility.getInstance().toJson(new AgentDO()));
					map.put("application", SerializationUtility.getInstance().toJson(new ApplicationDO()));
					map.put("ERROR_CD", hashMap.get("ERROR_CD"));
					String toJson = SerializationUtility.getInstance().toJson(map);
					LOGGER.info("Sending Response");
					return new ResponseEntity(toJson, reponseObject.getStatusCode());
				} else {
					LOGGER.info("Sending Response");
					return new ResponseEntity(reponseObject.getStatusCode());
				}
			}
		}
		LOGGER.info("Sending Response");
		return new ResponseEntity(reponseObject.getStatusCode());
	}

	@CrossOrigin
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	String logoutRequest(HttpServletRequest request) {
		
		String interfaceUrl = applicationRepository.findByKey(InsureConnectConstants.Path.TOKENINVALIDATIONPATH).getValue();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.set("Authorization",request.getHeader("Authorization"));
		HttpEntity<String> entity = new HttpEntity<String>("", httpHeaders);
		ResponseEntity<String> reponseObject = restTemplate.exchange(interfaceUrl, HttpMethod.GET, entity, String.class);
		if (HttpStatus.OK.equals(reponseObject.getStatusCode())) {
			return "{\"responsedata\":\"success\"}";
		}
		return null;
	}
		
	
	
}
