package com.metamorphosys.user.controller;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

@Component
public class EncryptDecryptClass {

	
	 public static String encrypt(String key, String initVector, String value) {
	        try {
	            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

	            byte[] encrypted = cipher.doFinal(value.getBytes());

	            return Base64.encodeBase64String(encrypted);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return null;
	    }

	    public static String decrypt(String key, String initVector, String encrypted) {
	        try {
	           IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
	            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

	            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

	            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

	            return new String(original);
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

	        return null;
	    }
	    
	    private String generateIv() {
	    	 SecureRandom random = new SecureRandom();
	            byte iv[] = new byte[16];//generate random 16 byte IV AES is always 16bytes
	            random.nextBytes(iv);
	           // IvParameterSpec ivspec = new IvParameterSpec(iv);
	            return base64(iv);
	    }

	    /**
	     * Generates a secret key to be used in the encryption process
	     * @return The secret key
	     */
	    private String generateKey() {
	    	 KeyGenerator keygen;
			try {
				keygen = KeyGenerator.getInstance("AES");
				 keygen.init(128);  // To use 256 bit keys, you need the "unlimited strength" encryption policy files from Sun.
		          byte[] key = keygen.generateKey().getEncoded();
		         //  SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
		           return base64(key);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;   
	    }
	    
	    public static String base64( byte[] bytes ) {
	        return Base64.encodeBase64String( bytes );
	    }

	    public static byte[] base64( String str ) {
	        return Base64.decodeBase64( str );
	    }

	    public static String hex( byte[] bytes ) {
	        return Hex.encodeHexString( bytes );
	    }

	    public static byte[] hex( String str ) {
	        try {
	            return Hex.decodeHex( str.toCharArray() );
	        } catch ( DecoderException e ) {
	            throw new IllegalStateException( e );
	        }
	    }
}
