package com.metamorphosys.user.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metamorphosys.insureconnect.dataobjects.master.RolePrivilegeMapDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.UsersDO;
import com.metamorphosys.insureconnect.jpa.master.LabelRepository;
import com.metamorphosys.insureconnect.jpa.master.RolePrivilegeMapRepository;
import com.metamorphosys.user.service.SecurityService;

@Controller
public class MenuController {

	@Autowired
	SecurityService securityService;

	@Autowired
	RolePrivilegeMapRepository rolePrivilegeMapRepository;
	
	@Autowired
	LabelRepository labelRepository;
	@CrossOrigin
	@RequestMapping(value = "/menu", method = RequestMethod.GET)
    public ResponseEntity getMenuList(HttpServletRequest request) {
		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		
		UsernamePasswordAuthenticationToken userDetails = securityService.getUserDetails();
		
		List<RolePrivilegeMapDO> rolePrivilegeMapDOs = null;
		for(GrantedAuthority grantedAuthority :userDetails.getAuthorities()){
			rolePrivilegeMapDOs =rolePrivilegeMapRepository.findByRoleIdAndPrivilegeTypeOrderBySortOrderAsc(grantedAuthority.getAuthority(), "MENU");
		}
		UsersDO usersDO=(UsersDO) request.getSession().getAttribute("user");

	/*	UsersDO usersDO=(UsersDO) request.getAttribute("user");

		for(RolePrivilegeMapDO rolePrivilegeMapDO:rolePrivilegeMapDOs){
			LabelDO label=labelRepository.findByLabelKeyAndLanguage(rolePrivilegeMapDO.getPrivilegeCd(),usersDO.getLanguage());
			rolePrivilegeMapDO.setPrivilegeCd(label.getLabelValue());
		}*/
		return new ResponseEntity(rolePrivilegeMapDOs, httpHeaders, httpStatus);
	}
}
