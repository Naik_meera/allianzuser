package com.metamorphosys.user.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metamorphosys.insureconnect.dataobjects.master.RoleDO;
import com.metamorphosys.insureconnect.jpa.master.RoleRepository;

@Controller
@RequestMapping("/role")
public class RoleController {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);

	@Autowired
	RoleRepository roleRepository;

	@CrossOrigin
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	ResponseEntity fetchByrulenameandservice() {

		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		LOGGER.info("Fetch All Roles......");
		List<RoleDO> roleDOlist = (List<RoleDO>) roleRepository.findAll();

		return new ResponseEntity(roleDOlist, httpHeaders, httpStatus);
	}
}