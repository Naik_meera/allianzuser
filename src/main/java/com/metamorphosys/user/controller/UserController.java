package com.metamorphosys.user.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

@Controller
public class UserController {
	/*
	 * @Autowired private UserService userService;
	 * 
	 * @Autowired private SecurityService securityService;
	 * 
	 * @Autowired private UserValidator userValidator;
	 * 
	 * @RequestMapping(value = "/registration", method = RequestMethod.GET)
	 * public String registration(Model model) { model.addAttribute("userForm",
	 * new UserDO());
	 * 
	 * return "registration"; }
	 * 
	 * @RequestMapping(value = "/registration", method = RequestMethod.POST)
	 * public String registration(@ModelAttribute("userForm") UserDO userForm,
	 * BindingResult bindingResult, Model model) {
	 * userValidator.validate(userForm, bindingResult);
	 * 
	 * if (bindingResult.hasErrors()) { return "registration"; }
	 * 
	 * userService.save(userForm);
	 * 
	 * securityService.autologin(userForm.getUsername(),
	 * userForm.getPasswordConfirm());
	 * 
	 * return "redirect:/welcome"; }
	 * 
	 * @RequestMapping(value = "/success", method = RequestMethod.GET) public
	 * String login(Model model, String error, String logout) { if (error !=
	 * null) model.addAttribute("error",
	 * "Your username and password is invalid.");
	 * 
	 * if (logout != null) model.addAttribute("message",
	 * "You have been logged out successfully.");
	 * 
	 * return "success"; }
	 */

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String menu() {
		return "user/menu";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)

	public String login(Model model, String error, String logout,HttpServletRequest request) {

		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null){

			try{
				request.getSession().invalidate();
				model.addAttribute("message", "You have been logged out successfully.");
			}catch(Exception e){
				e.printStackTrace();
			}

		}

		return "user/login";
	}
	
	public boolean invalidateUserSession(String userId,SessionRegistryImpl sessionRegistry) {

	    List<Object> principalsList = sessionRegistry.getAllPrincipals();
	    Object targetPrincipal = null;
	    for (Object principal : principalsList) {
	        if (principal instanceof UserDetails) {
	            if (((UserDetails) principal).getUsername().equals(userId)) {
	                targetPrincipal = principal;
	                break;
	            }
	        }
	    }
	    if (targetPrincipal == null) {
	        return false;
	    }

	    List<SessionInformation> userSessionsList = sessionRegistry.getAllSessions(targetPrincipal, false);

	    for (SessionInformation x : userSessionsList) {
	        x.expireNow();
	    }

	    return true;
	}

	@RequestMapping(value = "/route/**", method = RequestMethod.GET)
	public String route(HttpServletRequest request) {
		//pageName= pageName.replace("+", "/");
		String pageName=(String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		pageName= pageName.replace("/route", "");
		return pageName;
	}
	@RequestMapping(value = "/signout", method = RequestMethod.POST)
	public String signOut(Model model, String error, HttpServletRequest request) {
		request.getSession().invalidate();
		model.addAttribute("message", "Anna");
		return "user/login";
	}
	@RequestMapping(value = "/loginFailure", method = RequestMethod.GET)
	public String loginFailure(ModelMap model, HttpServletRequest request) {
		request.getSession().invalidate();
		return "user/loginFailure";
	}
}