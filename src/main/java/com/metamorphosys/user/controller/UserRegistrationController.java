package com.metamorphosys.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metamorphosys.insureconnect.dataobjects.transaction.UsersDO;
import com.metamorphosys.insureconnect.jpa.transaction.UserRepository;

@RestController
@RequestMapping("/userRegistration")
public class UserRegistrationController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRegistrationController.class);

	@Autowired
	UserRepository userRepository;
	@Autowired
	EncryptDecryptClass encryptDecryptClass;

//	@Autowired
//	JavaMailSender javaMailSender;

	@Autowired
	VelocityEngine velocityEngine;

	@CrossOrigin
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	ResponseEntity fetchByrulenameandservice() {

		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		//LOGGER.info("Fetch All Online users......");
		String userType = "Online";

		List<UsersDO> usersDO = (List<UsersDO>) userRepository.findByUserType(userType);
		
		return new ResponseEntity(usersDO, httpHeaders, httpStatus);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity save(@RequestBody @Valid UsersDO usersDO) {

		HttpStatus httpStatus = HttpStatus.OK;
		HttpHeaders httpHeaders = new HttpHeaders();
		// httpHeaders.setAccessControlAllowOrigin("*");
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

		//LOGGER.info("save MetadataController");
		
		String[] forKey = generatePassword().toString().split("-");
		String[] forIv = generatePassword().toString().split("-");
		String keySpec=forKey[0]+forKey[1]+forKey[2];
		String ivParameter=forIv[0]+forIv[1]+forIv[2];

		String[] parts = generatePassword().toString().split("-");
		String password=parts[4] + parts[1];
		String UserName=usersDO.getUserId();
		String emailId=usersDO.getEmailId();
		String encrypted=encryptDecryptClass.encrypt(keySpec, ivParameter, password);
		String encryptedPassword=keySpec+"__babacd_dcabab__"+ivParameter+"__babacd_dcabab__"+encrypted;
		usersDO.setPassword(encryptedPassword);//key+iv+pass

		UsersDO data = userRepository.save(usersDO);
		send1(UserName,password,emailId);
		return new ResponseEntity(data, httpHeaders, httpStatus);
	}

	UUID generatePassword() {
		UUID uuid;
		uuid = UUID.randomUUID();
		return uuid;

	}

	private void send1(String UserId,String password,String emailId) {
		Map model = new HashMap();
		model.put("userId", UserId);
		model.put("passwoed", password);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/templates/registration-confirmation-New.vm", "UTF-8", model);

//		MimeMessage mail = javaMailSender.createMimeMessage();
//		try {
//			MimeMessageHelper message = new MimeMessageHelper(mail, true);
//			message.setTo(emailId);
//			message.setFrom("anna.edzeal@gmail.com");
//			message.setSubject("Welcome to Metamorphosys Technologies !!!");
//			message.setText(text, true);
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		} finally {
//		}
//		javaMailSender.send(mail);
	}
}
