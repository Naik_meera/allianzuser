package com.metamorphosys.user.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.user.remoteservice.TokenAuthenticationService;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TokenFilterSecurityInterceptor implements Filter {

	private RequestMatcher toCheck = null;
	
	@Autowired
	TokenAuthenticationService tokenAuthenticationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenFilterSecurityInterceptor.class);
	public void init(FilterConfig filterConfig) throws ServletException {

		RequestMatcher tranferPolicyMatcher = new AntPathRequestMatcher("/api/transferPolicy", "POST");
		RequestMatcher aliTranferPolicyMatcher = new AntPathRequestMatcher("/api/ali/transferPolicy", "POST");
		RequestMatcher uploadActivityMatcher = new AntPathRequestMatcher("/activity/upload", "POST");
		RequestMatcher downloadActivityMatcher = new AntPathRequestMatcher("/activity/download", "GET");
		RequestMatcher vehicleSyncActivityMatcher = new AntPathRequestMatcher("/activity/syncVehicles/**", "GET");
		RequestMatcher downloadDBActivityMatcher = new AntPathRequestMatcher("/download/sqlite", "GET");
		toCheck = new OrRequestMatcher(tranferPolicyMatcher,aliTranferPolicyMatcher, uploadActivityMatcher, downloadActivityMatcher,downloadDBActivityMatcher,vehicleSyncActivityMatcher);
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String sessionid = ((HttpServletRequest) request).getSession().getId();
		httpServletResponse.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + ";secure;HttpOnly");
		if("OPTIONS".equals(httpRequest.getMethod())){
			
			httpServletResponse.setHeader("Access-Control-Allow-Origin", httpRequest.getHeader("Origin"));
			httpServletResponse.setHeader("Access-Control-Allow-Methods", "OPTIONS");
			httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
			//String header=null;
			httpServletResponse.setHeader("Access-Control-Allow-Headers", httpRequest.getHeader("access-control-request-headers"));
			httpServletResponse.addHeader("Access-Control-Expose-Headers", "xsrf-token");
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			//chain.doFilter(request, response);
		}else if (toCheck.matches(httpRequest)) {
			// "check token, and allow only if token valid"
			String headerNames = httpRequest.getHeader("agentid");
			String authToken=httpRequest.getHeader("Authorization");
			LOGGER.info("Validating Token");
			if(!tokenAuthenticationService.isValid(authToken)) {
				((HttpServletResponse) response).setStatus(HttpServletResponse.SC_FORBIDDEN);
			}else {
				LOGGER.info("Token is Valid");
				if (headerNames != null) {
					// "check token, and allow only if token valid"
					String source = httpRequest.getHeader("source");
					if (source.equals(InsureConnectConstants.Source.MOBILE)) {
						chain.doFilter(request, response);
					} else {
						((HttpServletResponse) response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
					}
				} else {
					((HttpServletResponse) response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
		} else {
			chain.doFilter(request, response);
		}
	}
	
	public void destroy() {

	}
}