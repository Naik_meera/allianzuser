package com.metamorphosys.user.remoteservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.transferobjects.TokenValidationResponseTO;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;

@Component
public class TokenAuthenticationService {
	
	private static final Logger log = LoggerFactory.getLogger(TokenAuthenticationService.class);
	
	@Autowired
	ApplicationRepository applicationRepository;
	
	@Autowired
	RestTemplate restTemplate;
	
	public boolean isValid(String token){
		try {
			String url =applicationRepository.findByKey(InsureConnectConstants.Path.TOKENVALIDATIONPATH).getValue();
			//restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Authorization",token);
			HttpEntity<String> entity = new HttpEntity<String>("", httpHeaders);
			log.info("Sending request to "+url);
			TokenValidationResponseTO responseObject = restTemplate.postForObject(url,entity,TokenValidationResponseTO.class);
			//ResponseEntity<String> responseObject = restTemplate.exchange(url,HttpMethod.GET, entity, String.class);
			//TokenValidationResponseTO responseTO= (TokenValidationResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), TokenValidationResponseTO.class);
			if(responseObject.getTokenStatus().equals(TokenValidationResponseTO.VALID)){
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
