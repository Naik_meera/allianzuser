package com.metamorphosys.user.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class SecurityUtil {

    private final int    keySize;
    private final int    iterationCount;
    private final Cipher cipher;

    public SecurityUtil( int keySize ) throws NoSuchPaddingException {
        this( keySize, 1000 );
    }

    public SecurityUtil( int keySize, int iterationCount ) throws NoSuchPaddingException {
        this.keySize = keySize; //128
        this.iterationCount = iterationCount; //1000
        try {
            cipher = Cipher.getInstance( "AES/CBC/PKCS5Padding" );
        } catch ( NoSuchAlgorithmException  e ) {
            throw fail( e );
        }
    }

    public String encrypt( String salt, String iv, String passphrase, String plaintext ) throws InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
        try {
            SecretKey key = generateKey( salt, passphrase );
            byte[] encrypted = doFinal( Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes( "UTF-8" ) );
            return base64( encrypted );
        } catch ( UnsupportedEncodingException e ) {
            throw fail( e );
        }
    }

    public String decrypt( String salt, String iv, String passphrase, String ciphertext ) throws InvalidKeySpecException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        try {
            SecretKey key = generateKey( salt, passphrase );
            byte[] decrypted = doFinal( Cipher.DECRYPT_MODE, key, iv, base64( ciphertext ) );
            return new String( decrypted, "UTF-8" );
        } catch ( UnsupportedEncodingException e ) {
            throw fail( e );
        }
    }

    private byte[] doFinal( int encryptMode, SecretKey key, String iv, byte[] bytes ) throws InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        try {
            cipher.init( encryptMode, key, new IvParameterSpec( hex( iv ) ) );
            return cipher.doFinal( bytes );
        } catch ( InvalidKeyException e ) {
            throw fail( e );
        }
    }

    private SecretKey generateKey( String salt, String passphrase ) throws InvalidKeySpecException {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA1" );
            KeySpec spec = new PBEKeySpec( passphrase.toCharArray(), hex( salt ), iterationCount, keySize );
            SecretKey key = new SecretKeySpec( factory.generateSecret( spec ).getEncoded(), "AES" );
            return key;
        } catch ( NoSuchAlgorithmException  e ) {
            throw fail( e );
        }
    }

    public static String random( int length ) {
        byte[] salt = new byte[length];
        new SecureRandom().nextBytes(salt);
        return hex( salt );
    }

    public static String base64( byte[] bytes ) {
        return Base64.encodeBase64String( bytes );
    }

    public static byte[] base64( String str ) {
        return Base64.decodeBase64( str );
    }

    public static String hex( byte[] bytes ) {
        return Hex.encodeHexString( bytes );
    }

    public static byte[] hex( String str ) {
        try {
            return Hex.decodeHex( str.toCharArray() );
        } catch ( DecoderException e ) {
            throw new IllegalStateException( e );
        }
    }

    private IllegalStateException fail( Exception e ) {
        return new IllegalStateException( e );
    }
}
