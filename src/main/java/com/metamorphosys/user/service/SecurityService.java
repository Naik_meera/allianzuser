package com.metamorphosys.user.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public interface SecurityService {
//String findLoggedInUsername();
//void autologin(String username, String password);

UserDetails getCurrentUser();

UsernamePasswordAuthenticationToken getUserDetails();
}
