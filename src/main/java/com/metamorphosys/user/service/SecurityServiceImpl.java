package com.metamorphosys.user.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {
	/*
	 * @Autowired private AuthenticationManager authenticationManager;
	 * 
	 * @Autowired private UserDetailsService userDetailsService;
	 * 
	 */ private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

	public UserDetails getCurrentUser() {
		Object userDetails = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (userDetails instanceof UserDetails) {
			return ((UserDetails) userDetails);
		}

		return null;
	}

	/*
	 * @Override public void autologin(String username, String password) {
	 * UserDetails userDetails =
	 * userDetailsService.loadUserByUsername(username);
	 * UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
	 * new UsernamePasswordAuthenticationToken(userDetails, password,
	 * userDetails.getAuthorities());
	 * 
	 * authenticationManager.authenticate(usernamePasswordAuthenticationToken);
	 * 
	 * if (usernamePasswordAuthenticationToken.isAuthenticated()) {
	 * SecurityContextHolder.getContext().setAuthentication(
	 * usernamePasswordAuthenticationToken); logger.debug(String.format(
	 * "Auto login %s successfully!", username)); } }
	 */

	public UsernamePasswordAuthenticationToken getUserDetails() {
		Object userDetails = SecurityContextHolder.getContext().getAuthentication();
		if (userDetails instanceof UsernamePasswordAuthenticationToken) {
			return ((UsernamePasswordAuthenticationToken) userDetails);
		}

		return null;
	}
}
