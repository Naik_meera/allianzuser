var userRegApp = angular.module('userRegApp', []);

userRegApp.controller('userRegistrationController', function($scope,CommonUtil) {
	console.log("UserReg js loaded successfully");
	$scope.user_roles=[];
// *****************************************************************	
	 $scope.addNewUserRole = function() {
		  var user_role=CommonUtil.getNewUserRole();
		  $scope.user_roles.push(user_role);
	  };

	  $scope.removeUserRole = function(item) { 
		  var index = $scope.user_roles.indexOf(item);
		  $scope.user_roles.splice(index, 1); 
	  };
//*****************************************************************		
});

userRegApp.service('CommonUtil', function($http,$q) {
	var self=this;
//*****************************************************************	
	self.getNewUserRole=function(){	
		var user_role={"role_type":"Admin"};
		return user_role
	};
//*****************************************************************		
});
